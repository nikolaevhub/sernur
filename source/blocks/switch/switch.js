class Switch {
	constructor() {
		this.selectors = {
			el: '.js-switch',
			native: '.js-switch-native',
			item: '.js-switch-item'
		};

		this.classes = {
			selected: 'switch__item_selected'
		};

		this.data = {
			eventChange: 'event-change',
			eventGet: 'event-get'
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({native, item}, {eventChange, eventGet}) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$native: $el.find(native),
				$items: $el.find(item),
				data: {
					change: $el.data(eventChange) || 'switch:change',
					get: $el.data(eventGet) || 'switch:get'
				}
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(elem => {
			elem.$el.on('switch:update', {that: this, elem}, this.setActive);

			elem.$items.on('click', {that: this, elem}, this.change);
			elem.$el.on(elem.data.get, {that: this, elem}, this.getData);
		});
	}

	setActive(e, data) {
		const 	{that, elem} = e.data,
				{id} = data;

		that.change({data:{that, elem}}, {id});
	}

	getData(e) {
		const	{elem, that} = e.data,
				$active = elem.$native.filter('input:checked'),
				value = $active.val(),
				data = $active.data();

		elem.$el.trigger(elem.data.change, {value, data});
	}

	change(e, activeId) {
		const	{elem, that} = e.data,
				sel = that.classes.selected;
		let id;

		if(activeId !== undefined) {
			id = activeId.id;
		}
		else {
			id = $(e.currentTarget).data('id');
		}

		elem.$native.each((i, el) => {
			const $el = $(el);

			$el.prop('checked', i == id);
			if(i == id){
				const data = $el.data();
				elem.$el.trigger(elem.data.change, {value: $el.val(), data});
			}
		});
		elem.$items.each((i, el) => $(el).toggleClass(sel, i == id));
	}

}