class SavedAddr {
	constructor() {
		this.selectors = {
			el: '.js-saved-addr',
			field: '.js-field'
		};

		this.classes = {
		};

		this.data = {
			eventDelete: 'event-delete'
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({field}, {eventDelete}) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$field: $el.find(field),
				data: {
					eventDelete: $el.data(eventDelete) || 'savedAddr:delete'
				}
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			el.$field.on('field:clear', {that: this, el}, this.fieldDelete);
		});
	}

	fieldDelete(e, {value, label}) {
		const	{that, el} = e.data,
				{eventDelete} = el.data;

		$(e.currentTarget).parent().remove();

		el.$el.trigger(eventDelete, {value, label});
	}

	// setSuggestion(e, data) {
	// 	const	{that, el} = e.data;

	// 	el.data.suggestions = el.$address.suggestions({
	// 		token: "670831253184a6570dcfb29298d37c4800de6d75",
	// 		type: "ADDRESS",
	// 		constraints: [
	// 			{locations: {kladr_id: 50}},
	// 			{locations: {kladr_id: 77}},
	// 			{locations: {kladr_id: 47}},
	// 			{locations: {kladr_id: 78}}
	// 		],
	// 		onSelect: (suggestion) => {
	// 			console.log(suggestion);
	// 			// that.getDeliveryPrice(el);
	// 		}
	// 	}).suggestions();
	// }
}
