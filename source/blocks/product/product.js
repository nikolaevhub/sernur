class Product {
	constructor() {
		this.selectors = {
			el: '.js-product',
			add: '.js-product-add',
			remind: '.js-product-remind',
			buttonText: '.js-product-button-text'
		};

		this.classes = {
			envelopeSuccess: 'product__name-envelope_success',
		};

		this.data = {
			url: 'url',
			eventAdd: 'event-add',
		};
		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({add, remind, buttonText}, {eventAdd, url}) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$add: $el.find(add),
				$remind: $el.find(remind),
				$buttonText: $el.find(buttonText),
				data: {
					url: $el.data(url),
					eventAdd: $el.data(eventAdd) || 'cart:add',
				}
			};
		});
		return this;
	}

	init() {
		this.$window.on('product:reload', {that: this}, this.reload);

		this.elems.forEach(el => {
			el.$add.on('click', {that: this, el}, this.addToCart);
			el.$remind.on('click', {that: this, el}, this.remindWhenArrive);
			el.$remind.data() && el.$remind.data().success && this.toggleRemindButton({that: this, el});
		});
	}

	reload(e) {
		const	{that} = e.data;

		that.$window.off('product:reload', this.reload);
		that.elems = [];
		that.el = $(that.selectors.el);

		that.get(that.selectors, that.data)
				.init();
	}

	addToCart(e) {
		e.preventDefault();

		const	{that, el} = e.data,
				{$add} = el,
				{url} = el.data,
				data = $add.data(),
				callback = that.addResponce,
				context = {that, el};

		that.$window.trigger('ajax:get', {url, data: $.param(data), callback, context});

	}

	remindWhenArrive(e) {
		e.preventDefault();

		const {that, el} = e.data,
			{$remind} = el,
			data = $remind.data();

		el.$remind.prop('disabled', true);

		$.ajax({type: 'POST', url: '/netcat/modules/default/actions/item_arrived_notifier.php', data: $.param(data)}).done(function() {
			that.toggleRemindButton({that, el});
		}).fail(function() {
			$('.subscribe-overlay').data('cc', data.cc).data('msg', data.msg).toggleClass('subscribe-overlay_open', true);
			console.log($('.subscribe-overlay').data());
		  });
	};

	toggleRemindButton(data) {
		const {that, el} = data;
		
		el.$remind.toggleClass(that.classes.envelopeSuccess);
		if (el.$remind.hasClass(that.classes.envelopeSuccess)) {
			el.$remind.off('click');
			el.$buttonText.text('Сообщим Вам\nо поступлении');
		} else {
			el.$remind.on('click', {that, el}, that.remindWhenArrive);
			el.$buttonText.text('Уведомить\nо поступлении');
		}
	}

	addResponce(data) {
		const	{that, el} = data.context;

		that.$window.trigger(el.data.eventAdd, data.data);
	}
}
