class Order {
	constructor() {
		this.selectors = {
			el: '.js-order',
			scale: '.js-scale',
			btn: '.js-button',
			footer: '.js-footer',
			summ: '.js-summ',
			totalSumm: '.js-total-summ',
			totalMass: '.js-total-mass',
			cartItem: '.js-cart-item',
			deliveryPrice: '.js-delivery-price',
			orderPayment: '.js-order-payment',
			// deliveryRest: '.js-delivery-rest',
			deliveryRestScale: '.js-scale',
			discount: {
				discount: '.js-order-discount',
				bonuses: '.js-order-discount-bonuses',
				card: '.js-order-discount-card',
				stock: '.js-order-discount-stock'
			}
		};

		this.classes = {
			fixed: 'order_fixed',
			sumErr: 'order_sum-err'
		};

		this.data = {
			payment: 'event-payment',
			minGoodsSum: 'min-goods-summ',
			cardDiscount: 'card-discount',
			// deliveryFree: 'delivery-free'
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);
		this.$document = $(document);
		this.$footer = this.$document.find(this.selectors.footer);

		this.desktopWidth = 1000;

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({scale, btn, summ, totalSumm, totalMass, cartItem, deliveryPrice, orderPayment, discount}, {payment, minGoodsSum,cardDiscount}) {
		// deliveryRest, deliveryRestScale
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			const curSumm = parseFloat($el.find(summ).text().replace(',','.').replace(/[^\d\.]/gi, ''));
			const minSumm = $el.data(minGoodsSum) || 0;
			return {
				$el,
				$scale: $el.find(scale),
				$btn: $el.find(btn),
				$summ: $el.find(summ),
				$totalSumm: $el.find(totalSumm),
				$totalMass: $el.find(totalMass),
				$deliveryPrice: $el.find(deliveryPrice),
				$orderPayment: $el.find(orderPayment),
				discount: {
					$discount: $el.find(discount.discount),
					$bonuses: $el.find(discount.bonuses),
					$card: $el.find(discount.card),
					$stock: $el.find(discount.stock)
				},
				// $deliveryRest: $el.find(deliveryRest),
				// $deliveryRestScale: $el.find(deliveryRestScale),
				data: {
					payCard: true,
					eventPayment: $el.data(payment) || 'order:payment',
					cardDiscount: $el.data(cardDiscount) || 0,
					minGoodsSum: minSumm,
					bonuses: 0,
					promo: [],
					delivery: {},
					items: {},
					moreMinSum: curSumm > minSumm,
					cardDiscountSumm: 0,
					stock: $el.data('stock').stock || [],
					stockSum: $el.data('stock').stockSum ||0,
					promoDiscount: [],
					// deliveryFree: $el.data(deliveryFree) || 100500
				}
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			this.setPosition({data: {that: this, el}});

			this.$window
				.on('resize, scroll', {that: this, el}, this.setPosition)
				.on('cart:update', {that: this, el}, this.goodsChange)
				.on('cart:delivery', {that: this, el}, this.deliveryChange)
				.on('cart:bonuses', {that: this, el}, this.useBonuses)
				.on('cart:submit', {el}, this.disableBtn)
				.on('cart:promo', {that: this, el}, this.applyPromo);

			el.$orderPayment.on('switch:change', {that: this, el}, this.paymentChange);
			el.$btn.on('click', {that: this, el}, this.validateForm);
		});
	}

	disableBtn(e, data) {
		e.data.el.$btn.prop('disabled', true);
	}

	applyPromo(e, data) {
		const	{that, el} = e.data,
				{type, value, percent, id, remove, message} = data;

		if(remove === undefined) {
			el.data.promo[id] = {type, value, percent, message};
		}
		else {
			el.data.promo.splice(id, 1);
		}

		that.setDelivery(el, that);
		that.setTotal(el);
	}

	validateForm(e) {
		const	{that, el} = e.data,
				{sumErr} = that.classes;

		if (el.data.moreMinSum) {
			e.preventDefault();
			that.$window.trigger('cart:validate');
		}
		else {
			console.log('Goods sum is to small');
		}

		el.$el.toggleClass(sumErr, !el.data.moreMinSum);
	}

	useBonuses(e, data) {
		const	{that, el} = e.data,
				{used} = data;

		el.data.bonuses = used;
		that.setTotal(el);
	}

	paymentChange(e, data) {
		const	{that, el} = e.data,
				{value} = data,
				url = el.$el.data('url');

		el.data.payCard = value == 2 ? true : false;

		that.$window.trigger(el.data.eventPayment, {value});
		that.$window.trigger('ajax:get', {url, data: `f_PaymentMethod=${value}`, callback: that.paymentUpdate, context: {that, el}});
	}

	paymentUpdate(data) {
		const {el, that} = data.context;

		that.goodsChange({data: {that, el}}, data.data);
	}

	goodsChange(e, data) {
		const	{that, el} = e.data,
				{TotalItemPrice, TotalMass, stock, stockSum} = data,
				{$summ, $totalSumm, $totalMass} = el;

		el.data.items.price = TotalItemPrice;
		el.data.stock = stock || [];
		el.data.stockSum = stockSum || 0;

		$summ.text(that.dot2comma(TotalItemPrice));
		$totalMass.text(TotalMass);

		that.$window.trigger('cart:deliveryRecalc');

		that.setTotal(el);
	}

	deliveryChange(e, data) {
		const	{that, el} = e.data,
				{price, deliveryFree} = data;

		el.data.delivery.price = price;
		el.data.delivery.free = deliveryFree;

		that.setDelivery(el, that);

	}

	setDelivery(el, that) {
		const	{$deliveryPrice, $deliveryRestScale} = el,
				{price, free} = el.data.delivery;
				 // $deliveryRest,
				// rest = (free > price) ? (free - price) : 0;
		let		total = price;

		el.data.promo.forEach((el, i) => {
			if(el.type == 'delivery') {
				total = 0;
			}
		});

		el.data.delivery.total = total;

		$deliveryPrice.text(total);
		// $deliveryRest.text(total === 0 ? 0 : rest);
		// $deliveryRestScale.trigger('scale:change', {total: free, value: free - rest});

		that.setTotal(el);
	}

	setDiscount(el) {
		const 	{discount} = el,
				{bonuses, cardDiscountSumm, stock, stockSum, promoDiscount} = el.data,
				promoTotal = promoDiscount.reduce(
					(sum, cur) => {
						return sum + cur.sum
					}, 0),
				total = bonuses + cardDiscountSumm + stockSum + promoTotal,
				allStock = stock.concat(promoDiscount);
		let 	html = "";

		discount.$bonuses.text((bonuses > 0 ? "−" : "") + this. dot2comma(bonuses));
		discount.$card.text((cardDiscountSumm > 0 ? "−" : "") +  this. dot2comma(cardDiscountSumm));

		if(allStock.length > 0) {
			allStock.forEach(item => {
				html += `
					<div class="order__discount-block">
						<div class="order__discount-type">${ item.name }
						</div>
						<div class="order__discount-amount">${ (item.sum > 0 ? "−" : "") + this.dot2comma(item.sum) } ₽
						</div>
					</div>
				`;
			});
			discount.$stock.html(html);
		}

		discount.$discount.text((total > 0 ? "−" : "") + this.dot2comma(total));

	}

	setTotal(el) {
		let	delivery = el.data.delivery.total || parseFloat(el.$deliveryPrice.text().replace(' ', '')),
			items = el.data.items.price || parseFloat(el.$summ.text().replace(' ', '')),
			total = items - el.data.bonuses,
			{cardDiscount} = el.data;

		el.data.moreMinSum = items >= el.data.minGoodsSum;

		el.data.promoDiscount = [];

		el.data.promo.forEach((item, i) => {
			if(item.type == 'discount') {
				let discount = 0;
				if(item.percent !== undefined) {
					discount = total * (item.value / 100);
					total -= discount;
				}
				else {
					discount = item.value;
					total -= item.value;
				}

				el.data.promoDiscount.push({
					name: item.message,
					sum: discount,
				});
			}
		});

		if(el.data.payCard) {
			el.data.cardDiscountSumm = this.rounding((total * cardDiscount) / 100);

			total = total - el.data.cardDiscountSumm;
		}
		else {
			el.data.cardDiscountSumm = 0;
		}

		el.$totalSumm.text(this.dot2comma(total + delivery));

		this.setDiscount(el);
	}

	rounding(num) {
		return Number.parseFloat(num.toFixed(2).toString());
	}

	dot2comma(num) {
		return num.toString().replace('.', ',');
	}

	setPosition(e) {
		const	{that, el} = e.data,
				{fixed} = that.classes,
				isMobile = that.$window.width() < that.desktopWidth;

		if(isMobile){
			el.$el.removeClass(fixed);
		}
		else {
			const 	elHeigth = el.$el.height(),
					dHeight = that.$document.height(),
					wHeight = that.$window.height(),
					scrollPos = that.$document.scrollTop(),
					footerH = that.$footer.height();

			const 	pos = (dHeight - wHeight) - scrollPos - footerH,
					hide = pos > elHeigth;

			el.$el.toggleClass(fixed, hide);
		}
	}
}
