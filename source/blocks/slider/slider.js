class Slider {
	constructor() {
		this.selectors = {
			el: '.js-slider',
			slider: '.js-slider-wrap',
			start: '.js-slider-start',
			end: '.js-slider-end'
		};

		this.classes = {
		};

		this.data = {
			min: 'min',
			max: 'max',
			start: 'start',
			end: 'end',
			eventChange: 'event-change',
			eventUpdate: 'event-update'
		}

		this.el = $(this.selectors.el);
		// this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({slider, start, end}, {min, max, start:startData, end:endData, eventChange, eventUpdate}) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$slider: $el.find(slider),
				$start: $el.find(start),
				$end: $el.find(end),
				data: {
					min: $el.data(min),
					max: $el.data(max),
					start: $el.data(startData),
					end: $el.data(endData),
					eventChange: $el.data(eventChange) || 'slider:change',
					eventUpdate: $el.data(eventUpdate) || 'slider:update'
				}
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			const	slider = el.$slider[0];
			this.setup(el);

			slider.noUiSlider.on('change', (data) => this.change(el, data));
			slider.noUiSlider.on('update', (data) => this.update(el, data));
		});
	}

	change(el, data) {
		const {$start, $end} = el;

		$start.val(data[0]);
		$end.val(data[1]);

		el.$el.trigger(el.data.eventChange, {start: data[0], end: data[1]});
	}

	update(el, data) {
		const {$start, $end} = el;

		$start.val(data[0]);
		$end.val(data[1]);

		el.$el.trigger(el.data.eventUpdate, {start: data[0], end: data[1]});
	}

	setup(el) {
		const	slider = el.$slider[0],
				{min, max, start, end} = el.data;

		noUiSlider.create(slider, {
				start: [start, end],
				step: 1,
				connect: true,
				range: {min, max},
				format:  {
					to: (val) => val,
					from:  (val) => val
				}
			});
	}
}
